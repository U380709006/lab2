public class FindGrade {
    static void FindGrade(int score){
        if (score <= 100 && score >= 90) {
            System.out.println("Your grade is A");
        } else if (score < 90  && score >= 80) {
            System.out.println("Your grade is B");
        } else if (score < 80 && score >=70){
            System.out.println("Your grade is C");
        } else if (score < 70 && score >=60){
            System.out.println("Your grade is D");
        } else if(score <60 && score >=0) {
            System.out.println("Your grade is F");
        } else {
            System.out.println("It is not a valid score");
        }
    }
    public static void main(String[] args){
        FindGrade(70);
        FindGrade(90);
        FindGrade(55);
        FindGrade(105);
        FindGrade(-5);
    }
}

