public class FindMin {
    static void FindMinimum(int number1, int number2, int number3 ){
        if (number1 <= number2 && number1 <= number3) {
            System.out.println(number1 + "is a small number");
        } else if (number2 <= number1 && number2 <= number3){
            System.out.println(number2 + "is a small number");
        }else {
            System.out.println(number3 + "is a small number");
        }
    }
    public static void main(String[] args){
        FindMinimum(3,5,2);
        FindMinimum(5,8,6);
        FindMinimum(9,7,7);
    }
}
